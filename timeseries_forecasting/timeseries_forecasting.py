from __future__ import division, print_function
import pandas as pd
import sys
from pyspark.sql import functions as f, types as t
from pyspark.sql import SparkSession, Window
from sklearn.model_selection import ParameterSampler
from fbprophet.diagnostics import performance_metrics
from fbprophet.diagnostics import cross_validation
import numpy as np
import datetime as dt
import re
import random
from fbprophet import Prophet



def prepare_df_to_prophet(df, dates_column, metric_column, additional_regressor=None, df_with_regressor=None):
    """function to prepare a spark dataframe for prophet 
    Attributes:
        df, dataframe
        metric_name: string
    Returns: panda dataframe with ds (datetime) and y(forecasted variable)
    """
    df_pd = df.select(dates_column, metric_column).toPandas()
    df_pd[metric_column] = df_pd[metric_column].astype('float')
    df_pd[dates_column]= pd.to_datetime(df_pd[dates_column])
    df_pd = df_pd.rename(columns = {metric_column: 'y', dates_column: 'ds'})
    df_pd = df_pd.sort_values('ds')
    
    if additional_regressor:
        df_pd = pd.merge(df_pd, df_with_regressor[['ds', additional_regressor]], how='inner', on='ds')
    
    return df_pd

class Forecasting(object):
    
    """Forcasting class for automated timeseries forecasting
    Atttributes:
    
        df : Spark DataFrame with histric values 
        dates_column : string ,Date Column in DataFrame
        metric_column : string, Metric to forecast
        growth :string, 'logistic' or 'linear'
        training_horizon : int; training horixon for cross validation and final forecast
        test_horizon : int, periods to evalute in cross-validation (cc)
        final_forecast_horizon: int; how days to forecast
        spacing_period: int, periods between cc validations
        n_iter: int, number of grids to test
        seed: int, seed
        additional_regressor: string, name of additional regressor column
        df_with_regressor: df, Data Frame having a time column ('ds') and the additional_regressor
        spark: spark_context, name of the spark_context
    
    """

    
    def __init__(self, df ,dates_column, metric_column, spark, growth='logistic', training_horizon=450, test_horizon=180, final_forecast_horizon=365, spacing_period=60, n_iter=10, seed=1234, additional_regressor=None, df_with_regressor=None):
        self.dates_column = dates_column
        self._transform = prepare_df_to_prophet
        self.metric_column = metric_column
        self.perf = self._transform(df, dates_column, metric_column, additional_regressor, df_with_regressor)
        self.growth = growth
        self.training_horizon = training_horizon
        self.test_horizon = test_horizon
        self.spacing_period = spacing_period
        self.final_forecast_horizon = final_forecast_horizon
        self.n_iter = n_iter
        self.additional_regressor = additional_regressor
        self.df_with_regressor = df_with_regressor
        self.seed = seed
        self.spark = spark
        
    def get_holidays_table(self, start_date='2017-01-01', end_date='2030-12-31'):
        """get the cs holiday table from start date of the forecasting data until last availability

        Args:
            start_date: string, start date of the forecasting data
            end_date: string, end date of holiday table

        Returns:
            panda dataframe with holidays
        """
        query = """
        SELECT
            yyyy_mm_dd AS ds,
            name AS holiday,
            upper(cc1) AS pos
        FROM
            csa.holidays_2000_2030_cc1
        WHERE
            yyyy_mm_dd BETWEEN '{start_date}' AND '{end_date}'
        AND
            level = 1

        """.format(start_date = start_date, end_date=end_date)
        df_holiday = self.spark.sql(query).toPandas()

        df_grouped_holidays = df_holiday.groupby(['ds', 'holiday']).count().reset_index()
        df_grouped_holidays['rank_holiday'] = df_grouped_holidays.groupby('ds')['pos'].rank(ascending = False, method='dense')

        # Only keep most important holiday per date (rank==1)
        #remove dates in case a date has multiple holidays (keep first, random)
        df_unfiltered_holidays = df_grouped_holidays[df_grouped_holidays.rank_holiday==1].drop_duplicates(subset='ds')

        #keep 90th percentile of most common holidays (90th can be adjusted based on fitting later)
        #No lower or upper windows needed since days before or after major events are already in the holiday table
        #For example ChristmasEve(24-12) and ChristmasDay(25-12)
        self.df_final_holidays=df_unfiltered_holidays[df_unfiltered_holidays.pos >= df_unfiltered_holidays['pos'].quantile(0.9)][['ds', 'holiday']]
    
        
    def base_forecast(self):
        """
        Get performance for base model
        Args:
        perf: df with metric to predict
        training_horizon, int: training horizon used in model evaluation
        spacing_period, int: timerinterval between cross validation folds
        test_horizon, int: horizon to forecast
        """

        m = Prophet(yearly_seasonality=True,weekly_seasonality=True,holidays=self.df_final_holidays, growth=self.growth)
        cap, floor = np.max(self.perf.y) * 1.1, np.min(self.perf.y) * 0.9
        self.perf['cap'] = cap
        self.perf['floor'] = floor
        if self.additional_regressor:
            m.add_regressor(self.additional_regressor)
        m.fit(self.perf)
        #Add regressor

        df_cv = cross_validation(m, initial="{0} days".format(self.training_horizon)
                                 , period="{0} days".format(self.spacing_period)
                                 , horizon = "{0} days".format(self.test_horizon))
        self.base_smape = float(np.mean(np.abs(df_cv.y-df_cv.yhat)/(np.abs(df_cv.y) + np.abs(df_cv.yhat))/2) * 100)
        
        
        
    def grid_search_spark_2(self):

        param_dict = {'changepoint_prior_scale' : [float(x) for x in (np.arange(0.01, 0.2,0.01))]
    #                   , 'growth': ['linear', 'logistic']
    #                   ,'seasonality_mode' : ['multiplicative', 'additive']
                      }

        params = list(ParameterSampler(param_dict, n_iter = self.n_iter, random_state=self.seed))

        params = pd.DataFrame(data=list(zip(list(params))), columns=['params'])

        #print(params)

        schema = t.StructType(
            [
                t.StructField('params',
                    t.StructType(
                        [t.StructField('changepoint_prior_scale', t.FloatType())
    #                      , t.StructField('growth', t.StringType())
    #                      , t.StructField('seasonality_mode', t.StringType())
                        ]

                    )
                )
            ]
        )


        df_best_params = self.spark.createDataFrame(params, schema)
        
       

        def create_model_evaluation_udf(perf_df, training_horizon, test_horizon, holiday_table, spacing_period, growth, seed, additional_regressor):
            """
            create_model_evaluation_udf is spark udf to do hyperparameter search with random grid search,
            it returns the symmetric mean absolute percentage error for combinations of train/test and
            parameter set combinations based on the desired aggregation level

            """


            def model_evaluation(params):
                random.seed(seed)
                params = params.asDict()

                cap, floor = np.max(perf_df.y) * 1.1, np.min(perf_df.y) * 0.9 

                perf_df['cap'] = cap
                perf_df['floor'] = floor

                model = Prophet(daily_seasonality = False,yearly_seasonality=True,weekly_seasonality=True,
                                holidays = holiday_table, growth=growth,**params)
                if additional_regressor:
                    model.add_regressor(additional_regressor)
                model.fit(perf_df)

                df_cv = cross_validation(model, initial="{0} days".format(training_horizon)
                                         , period="{0} days".format(spacing_period)
                                         ,horizon = "{0} days".format(test_horizon))
    #             df_p = performance_metrics(df_cv)
    #             avg_mape = df_p.mape.mean()
                avg_mape = float(np.mean(np.abs(df_cv.y-df_cv.yhat)/(np.abs(df_cv.y) + np.abs(df_cv.yhat))/2) * 100)


                return float(avg_mape)

            return f.udf(model_evaluation, t.FloatType())


        mod_eval_udf = create_model_evaluation_udf(perf_df=self.perf
                                                   , training_horizon=self.training_horizon
                                                   , test_horizon=self.test_horizon
                                                   , holiday_table=self.df_final_holidays
                                                   , spacing_period = self.spacing_period
                                                   , growth=self.growth
                                                   , seed = self.seed
                                                   , additional_regressor = self.additional_regressor
                                                   )


        df_best_params=df_best_params.withColumn('mape', mod_eval_udf(f.col("params"))).cache()
        df_best_params =df_best_params.toPandas()
        self.best_params_grid = df_best_params.sort_values(by=['mape'], ascending=True).iloc[0]['params'].asDict()
        self.best_smape_grid = df_best_params.sort_values(by=['mape'], ascending=True).iloc[0]['mape']
        
        
    def automated_forecast_full(self):
        """
        Select best model based on smape (base vs grid search) and fit final model to obtain 
        prediction for next 365 days
        Args:
        IN:
        perf, df: dataframe inlduing metric and ds (prophet suitable format)
        training_horizon, int: Training horizon
        base_smape, float: smape from base model
        grid_smape, float: smape from grid search
        grid_params, dict: best parameters from random search
        Out:
        df with predictions
        """
        self.get_holidays_table()
        self.base_forecast()
        self.grid_search_spark_2()
        

        df_to_forecast = self.perf[self.perf.ds >= max(self.perf.ds) - dt.timedelta(self.training_horizon)]


        if self.base_smape < self.best_smape_grid:

            self.m = Prophet(yearly_seasonality=True,weekly_seasonality=True,holidays=self.df_final_holidays, growth=self.growth)
        else:         
            self.m = Prophet(yearly_seasonality=True,weekly_seasonality=True,holidays=self.df_final_holidays, growth=self.growth, **self.best_params_grid)
        
        cap, floor = np.max(df_to_forecast.y) * 1.1, np.min(df_to_forecast.y) * 0.9
        df_to_forecast['cap'] = cap
        df_to_forecast['floor'] = floor
        if self.additional_regressor:
            self.m.add_regressor(self.additional_regressor)
        self.m.fit(df_to_forecast)
        future = self. m.make_future_dataframe(periods=self.final_forecast_horizon, freq='1D')
        future['floor'] = floor
        future['cap'] = cap
        if self.additional_regressor:
            future = pd.merge(future, self.df_with_regressor, how='left', on='ds') 
        self.forecast = self.m.predict(future)
        predictions = self.forecast.tail(self.final_forecast_horizon)[['ds','yhat', 'yhat_lower', 'yhat_upper']]
        predictions.rename(columns = {col : col+'_{}'.format(self.metric_column) for col in [column for column in predictions.columns[1:]]},inplace=True)
        predictions.rename(columns = {col : re.sub('yhat_', '', col).format(self.metric_column) for col in [column for column in predictions.columns[1:]]},inplace=True)
        self.predictions_full = predictions
        
    def automated_forecast_light(self):
        
        self.get_holidays_table()
        
        df_to_forecast = self.perf[self.perf.ds >= max(self.perf.ds) - dt.timedelta(self.training_horizon)]
        self.m = Prophet(yearly_seasonality=True,weekly_seasonality=True,holidays=self.df_final_holidays, growth=self.growth)
        cap, floor = np.max(df_to_forecast.y) * 1.1, np.min(df_to_forecast.y) * 0.9
        df_to_forecast['cap'] = cap
        df_to_forecast['floor'] = floor
        if self.additional_regressor:
            self.m.add_regressor(self.additional_regressor)
        self.m.fit(df_to_forecast)
        future = self.m.make_future_dataframe(periods=self.final_forecast_horizon, freq='1D')
        future['floor'] = floor
        future['cap'] = cap
        if self.additional_regressor:
            future = pd.merge(future, self.df_with_regressor, how='left', on='ds') 
        self.forecast = self.m.predict(future)
        predictions = self.forecast.tail(self.final_forecast_horizon)[['ds','yhat', 'yhat_lower', 'yhat_upper']]
        predictions.rename(columns = {col : col+'_{}'.format(self.metric_column) for col in [column for column in predictions.columns[1:]]},inplace=True)
        predictions.rename(columns = {col : re.sub('yhat_', '', col).format(self.metric_column) for col in [column for column in predictions.columns[1:]]},inplace=True)
        self.predictions_light = predictions
        
